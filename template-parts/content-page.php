<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _s
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php the_content(); ?>
	<!-- .entry-content -->

</article><!-- #post-## -->

<?php

  $link_bottom = get_post_custom_values('link_bottom');
  // foreach ( $link_bottom as $link => $value ) {
  //   echo "$key  => $value ( 'my_key' )<br />";
  // }

if( count($link_bottom) > 0 || is_front_page() ){

    $link_bottom = get_post_custom_values( 'link_bottom' ); ?>
    <a class="fp-down" href="<?php echo $link_bottom[0]  ?>"></a>
<?php }

?>
