<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _s
 */
?>
<h1><?php the_title(); ?></h1>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php the_content(); ?>
	<!-- .entry-content -->

</article><!-- #post-## -->
