<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
<meta name="description" content="<?php bloginfo('description'); ?>" />
<meta name="keywords" content="Novia, novias, vestidos de novia, trajes de novia, trajes de fiesta, vestidos de noche, alta costura, diseño, medida, moda,evening dresses, on line, store, tienda, compra, ventay" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<!-- <script src="//use.typekit.net/kng0kdi.js"></script>
<script>try{Typekit.load();}catch(e){}</script> -->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header role="header">
		<h1 class="logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 151 51" enable-background="new 0 0 151 51" xml:space="preserve"><style>.style0{fill:	#1D1D1B;}.style1{clip-path:	url(#SVGID_2_);fill:	#1D1D1B;}.style2{clip-path:	url(#SVGID_4_);fill:	#1D1D1B;}.style3{clip-path:	url(#SVGID_6_);fill:	#1D1D1B;}.style4{clip-path:	url(#SVGID_8_);fill:	#1D1D1B;}</style><g><polygon points="0,0.4 0,4.1 4.4,4.1 4.4,17.3 8.8,17.3 8.8,4.1 13.2,4.1 13.2,0.4" class="style0"/><polygon points="38.9,0.4 29.2,0.4 29.2,17.3 38.9,17.3 38.9,13.6 33.6,13.6 33.6,10.7 38.6,10.7 38.6,6.9 33.6,6.9 33.6,4.1 38.9,4.1" class="style0"/><g><defs><rect y="0" width="150.7" height="50"/></defs><clipPath><use xlink:href="#SVGID_1_" overflow="visible"/></clipPath><path d="M62.1 10.4c2.4-0.6 3.7-2.2 3.7-4.8c0-1.3-0.4-2.4-1.2-3.4 c-0.9-1.2-2.4-1.8-4.4-1.8h-6.8v16.9h4.4v-6.5l4.2 6.5h5.5L62.1 10.4z M57.7 3.8h0.8c1.7 0 2.7 0.8 2.7 2.1c0 1.3-1 2.1-2.7 2.1 h-0.8V3.8z" class="style1"/></g><polygon points="91.6,0.4 81.9,0.4 81.9,17.3 91.6,17.3 91.6,13.6 86.3,13.6 86.3,10.7 91.3,10.7 91.3,6.9 86.3,6.9 86.3,4.1 91.6,4.1" class="style0"/><g><defs><rect y="0" width="150.7" height="50"/></defs><clipPath><use xlink:href="#SVGID_3_" overflow="visible"/></clipPath><path d="M120.3 1.4C118.6 0.4 116.8 0 115 0c-1.7 0-3.1 0.5-4.2 1.6 c-1.1 1.1-1.6 2.4-1.6 4.1c0 1.5 0.4 2.6 1.3 3.4c0.5 0.4 1.6 0.9 3.2 1.3c2.1 0.6 2.6 1.1 2.6 2c0 1-0.8 1.7-2.1 1.7 c-1.3 0-2.6-0.6-3.9-1.7l-1.9 3.5c1.8 1.3 3.8 1.9 5.9 1.9c2.1 0 3.7-0.5 4.8-1.6c1.2-1.1 1.7-2.5 1.7-4.4c0-2.4-1.2-3.8-4.1-4.7 c-1.2-0.4-2-0.6-2.2-0.8c-0.5-0.3-0.8-0.7-0.8-1.1c0-0.4 0.2-0.8 0.5-1c0.4-0.3 0.8-0.4 1.3-0.4c1 0 1.9 0.4 2.9 1.1L120.3 1.4z" class="style2"/></g><path d="M141.9 5.6l1.8 5.4h-3.7L141.9 5.6z M139.5 0.4L133 17.3h4.7l1.2-2.9h6.1l1.1 2.9h4.7l-6.4-16.9H139.5z" class="style0"/><g><defs><rect y="0" width="150.7" height="50"/></defs><clipPath><use xlink:href="#SVGID_5_" overflow="visible"/></clipPath><path d="M141.9 43.5h3.6c-0.3 2-1.4 2.9-3.5 2.9c-1.3 0-2.4-0.5-3.2-1.5 c-0.9-1-1.3-2.3-1.3-3.8c0-1.5 0.4-2.8 1.3-3.8c0.8-1 1.9-1.5 3.1-1.5c1.7 0 2.9 0.9 3.6 2.7l4.2-1.7c-1.5-3.2-4.1-4.8-7.6-4.8 c-3 0-5.3 1-7 2.9c-1.5 1.7-2.2 3.7-2.2 6.2c0 2.7 0.8 4.8 2.5 6.5c1.7 1.6 3.9 2.5 6.5 2.5c1.9 0 3.6-0.5 5-1.4 c1.4-1 2.4-2.3 3-4c0.4-1.2 0.6-2.7 0.6-4.5h-8.7V43.5z" class="style3"/></g><rect x="112.6" y="32.5" width="4.4" height="16.9" class="style0"/><g><defs><rect y="0" width="150.7" height="50"/></defs><clipPath><use xlink:href="#SVGID_7_" overflow="visible"/></clipPath><path d="M89.9 49.4c3.6 0 5.8-1.8 5.8-4.8c0-1.2-0.3-2.2-0.9-2.9 c-0.5-0.6-1.3-1.1-2.6-1.3c1.2-0.7 1.9-1.9 1.9-3.5c0-2.9-1.7-4.4-4.8-4.4h-7.4v16.9H89.9z M86.3 35.9H88c1.4 0 2.1 0.5 2.1 1.6 c0 1.1-0.7 1.6-2.1 1.6h-1.7V35.9z M86.3 42.4h1.8c1.3 0 2.1 0.1 2.5 0.4c0.5 0.3 0.7 0.8 0.7 1.4c0 1.3-0.9 1.8-3.1 1.8h-1.8 V42.4z" class="style4"/></g><polygon points="55.8,32.5 55.8,49.4 65.9,49.4 65.9,45.7 60.2,45.7 60.2,32.5" class="style0"/><polygon points="29.2,32.5 29.2,49.4 38.9,49.4 38.9,45.7 33.6,45.7 33.6,42.8 38.6,42.8 38.6,39.1 33.6,39.1 33.6,36.2 38.9,36.2 38.9,32.5" class="style0"/><polygon points="8.8,32.5 8.8,39 4.4,39 4.4,32.5 0,32.5 0,49.4 4.4,49.4 4.4,42.4 8.8,42.4 8.8,49.4 13.2,49.4 13.2,32.5" class="style0"/></g></svg>
			</a>
		</h1>
        <icon class="toggle-menu"></icon>
        <icon class="toggle-gallery-in">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 27 20" enable-background="new 0 0 27 20" xml:space="preserve">
                <g>
                    <rect x="0.1" y="0.4" width="7.4" height="4.9"/>
                    <rect x="0.1" y="7.6" width="7.4" height="4.9"/>
                    <rect x="0.1" y="14.8" width="7.4" height="4.9"/>
                    <rect x="9.8" y="0.4" width="7.4" height="4.9"/>
                    <rect x="9.8" y="7.6" width="7.4" height="4.9"/>
                    <rect x="9.8" y="14.8" width="7.4" height="4.9"/>
                    <rect x="19.5" y="0.4" width="7.4" height="4.9"/>
                    <rect x="19.5" y="7.6" width="7.4" height="4.9"/>
                    <rect x="19.5" y="14.8" width="7.4" height="4.9"/>
                </g>
            </svg>
        </icon>
        <icon class="toggle-gallery-out"></icon>
        <?php do_action('wpml_add_language_selector'); ?>
	</header>
