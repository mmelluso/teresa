<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package _s
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

<nav role="navigation">
    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
    <ul class="social-links">
        <li><a href="https://www.facebook.com/teresahelbig.barcelona" target="_blank"><icon class="icon-facebook"></icon></a></li>
        <li><a href="https://twitter.com/teresahelbig" target="_blank"><icon class="icon-twitter"></icon></a></li>
        <li><a href="https://instagram.com/teresahelbig/" target="_blank"><icon class="icon-instagram"></icon></a></li>
        <li><a href="https://www.pinterest.com/teresahelbig/" target="_blank"><icon class="icon-pinterest"></icon></a></li>
        <li><a href="https://vimeo.com/channels/teresahelbig" target="_blank"><icon class="icon-vimeo"></icon></a></li>
        <!-- <li><a href="" target="_blank"><icon class="icon-linkedin2"></icon></a></li>
        <li><a href="" target="_blank"><icon class="icon-google-plus"></icon></a></li> -->
    </ul>
</nav>
