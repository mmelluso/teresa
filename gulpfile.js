/*global -$ */
'use strict';
// generated on 2015-03-16 using generator-gulp-webapp 0.3.0
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;


// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [
    './style.css',
    './*.php'
    ];

    //initialize browsersync
    browserSync.init(files, {
    //browsersync with a php server
    proxy: "test.teresa.com",
    notify: true
    });
});

// Sass task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('styles', function () {
  return gulp.src('sass/*.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'nested', // libsass doesn't support expanded yet
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe($.postcss([
      require('autoprefixer-core')({browsers: ['last 1 version']})
    ]))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.'))
    .pipe(reload({stream: true}));
});

gulp.task('jshint', function () {
  return gulp.src('js/*.js')
    .pipe(reload({stream: true, once: true}))
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.if(!browserSync.active, $.jshint.reporter('fail')));
});

gulp.task('html', ['styles'], function () {
  var assets = $.useref.assets({searchPath: ['.']});

  return gulp.src('./*.php')
    .pipe(assets)
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.csso()))
    .pipe(assets.restore())
    .pipe($.useref())
    //.pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
    .pipe(gulp.dest('fab'));
});

gulp.task('images', function () {
  return gulp.src('img/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    })))
    .pipe(gulp.dest('images'));
});

gulp.task('fonts', function () {
  return gulp.src(require('main-bower-files')({
    filter: '**/*.{eot,svg,ttf,woff,woff2}'
  }).concat('fonts/**/*'))
    //.pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('fonts'));
});

// gulp.task('serve', ['styles', 'browser-sync'], function () {
//   gulp.watch([
//     './*.php',
//     'assets/sass/**/*.sass',
//     'assets/js/**/*.js',
//     'assets/images/**/*',
//     'assets/fonts/**/*'
//   ]);
// });

gulp.task('serve', ['styles', 'fonts', 'browser-sync'], function () {
  // browserSync({
  //   notify: true,
  //   port: 9000,
  //   server: {
  //     baseDir: ['.'],
  //     routes: {
  //       '/bower_components': 'bower_components'
  //     }
  //   }
  // });

  // watch for changes
  // gulp.watch([
  //   './*.php',
  //   './js/**/*.js',
  //   './images/**/*',
  //   './fonts/**/*'
  // ]).on('change', reload);

  gulp.watch('sass/**/*.scss', ['styles']);
  gulp.watch('img/**/*', ['images']);
  //gulp.watch('assets/fonts/**/*', ['fonts']);
  gulp.watch('bower.json', ['wiredep', 'fonts']);
});

// inject bower components
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream;

  gulp.src('sass/*.scss')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('./styles'));

  // gulp.src('app/*.html')
  //   .pipe(wiredep({
  //     exclude: ['bootstrap-sass-official'],
  //     ignorePath: /^(\.\.\/)*\.\./
  //   }))
  //   .pipe(gulp.dest('app'));
});

gulp.task('build', ['jshint', 'images', 'fonts'], function () {
  return gulp.src('fab/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});


// Default task to be run with `gulp`
gulp.task('default', ['sass', 'browser-sync'], function () {
    gulp.watch("sass/**/*.scss", ['sass']);
});
