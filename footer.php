<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _s
 */
?>

<!-- footer -->
<!-- <script type="text/javascript">
    // Hide contents before fullpage render
    jQuery('#main > article').css('visibility', 'hidden');
</script> -->
<?php wp_footer(); ?>

</body>
</html>
