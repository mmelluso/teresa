( function( $ ) {

	var langLink = $('.lang_sel_other').attr('href');
    var url = document.URL;

	function stopActiveVideo(){
        var iframe = $('.fp-slide.active .vimeo iframe');
        if(iframe.length>0){
            var player = $f(iframe[0]);
            player.api('getCurrentTime', function (value, player_id) {
                if(value>0){
                    player.api('unload');
                }
            });
        }
    }

    function playActiveVideo(el){
		if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		    var iframe = el.find('.vimeo iframe');
		    var player = $f(iframe[0]);
		    player.addEvent('ready', function() {
		    	player.api('play');
		    });
		}
    }

    function updateLangLink(section, slide){
    	if(!slide){
    		slide = "";
    	}
    	var url = document.URL;
    	//console.log(url);
    	var sectionID = "#" + section + "/" + slide;
    	//console.log(newUrl);
    	//if( url.indexOf(section) < 1 && url.indexOf(newUrl) > 1 ){
	    	var newLangUrl = langLink + sectionID;
	    	//console.log(url);
	    	$('.lang_sel_other').attr('href', newLangUrl);
    	//}
    }

	$(document).ready(function() {

		var body = $('body');
		var isSafari = /constructor/i.test(window.HTMLElement);

		if(!isSafari){
			$('.so-widget-sow-image figure, .textwidget h1, .textwidget p').css('letterSpacing', '-1.5px');
		}

		/* Removing slide from DOM according to screen size
		----------------------------------*/
		$('.slide').each(function(){
			var length = $(this).find('.panel-grid-cell:eq(0)').parent().children().length
			if( length > 1 ){
				if( window.innerWidth < 1024 ) {
					$(this).remove();
				} else {
					if(length == 2) {
						$(this).nextAll().slice(0, 2).remove();
					} else if(length == 3){
						$(this).nextAll().slice(0, 3).remove();
					}
				}
				//$(this).addClass('toBeRemoved');
				//console.log('yo');
			}

			/* Removed sharing icons from class no-share
			----------------------------------*/
			// if( !$(this).find('.panel-row-style').hasClass('share-img') && !$(this).find('.panel-row-style').hasClass('press-gallery') ){
			// 	$(this).find('.panel-grid-cell').each(function(){
			// 		var img = $(this).find('.wwm_socialshare_imagewrapper img').detach();
			// 		$(this).find('.wwm_socialshare_imagewrapper').remove();
			// 		$(this).find('.so-widget-sow-image').append(img);
			// 	})
			// }
		})

		/* Recover slide numeration after removing them
		----------------------------------*/
		$('.section').each(function(){
	        var slideId = $(this).find('.slide:eq(0)').attr('id').match(/\w+-\w+-/);
	        $('.slide', this).each(function(index){
	        	$(this).attr('id', slideId+index);
	        })
		})

		/* Removing share div in unappropriate parts
		----------------------------------*/
		// if( $('.section').next('ul') ){
		// 	$('.section').next('ul').remove();
		// }

		/* Create page navigation
		----------------------------------*/
		$('#main > article').fullpage({
			loopHorizontal: false,
			keyboardScrolling: false,
			anchors:['firstPage', 'secondPage', 'thirdPage', 'fourthPage'],
	        afterRender: function(){
	        	//console.log( $('.slide.active .panel-row-style').hasClass('white') );
				if( $('.section.active .slide.active .panel-row-style').hasClass('white') ){
					body.addClass('black-ui');
				} else {
					body.removeClass('black-ui');
				}
				//console.log(url.split('#').length);
				if( url.split('#').length < 2){
					// Show Front Page After Render
					$('#main > article').css('visibility', 'visible');
				}
				// Play video if exists
				if( $('.section.active .slide.active .panel-row-style-video') ){
					playActiveVideo( $('.section.active .slide.active .panel-row-style-video') );
				}
				// Check if gallery and load Alt Gallery
				if( $('.section.active .slide.active').find('.wwm_socialshare_imagewrapper') ){
					//Check if it's a galley
					var isGallery;
					if( $('.section.active .slide.active').find('.wwm_socialshare_imagewrapper:eq(0)').parent().hasClass('so-widget-sow-image-base') ){
							isGallery = true;
					}

					if(isGallery){
						$('.toggle-gallery-in').css('visibility', 'visible');
						if(!$('#main > .alt-gallery').length){
							addAltGallery();
						}
					} else {
						$('.toggle-gallery-in').css('visibility', 'hidden');
					}
				}
	        },
	        onSlideLeave: function( anchorLink, index, slideIndex, direction, nextSlideIndex){
	            //var wrapperSlide = $(this).parent();
	            //console.log(index + ' ' + slideIndex + ' ' + nextSlideIndex);
	            var currentId = $(this).attr('id').match(/\w+-\w+-/);
	            var nextId = "#" + currentId[0] + nextSlideIndex;
	            //console.log(currentId[0] + " " + nextId);

	            //if( nextSlideIndex > slideIndex ){
		            if( $(this).parent().find(nextId).find('.panel-row-style').hasClass('white') ){
						body.addClass('black-ui');
					} else {
						body.removeClass('black-ui');
					}
					// Play video if exists in the next slide
					if( $(this).parent().find(nextId).find('.panel-row-style-video') ){
						playActiveVideo( $(this).parent().find(nextId).find('.panel-row-style-video') );
					}
		            // if( $(this).next().is(':last-child') ){
		            // 	$(this).parents('.panel-grid-cell').find('.fp-next').hide();
		            // }
	            //} else {
		   			//  if( $(this).prev().find('.panel-row-style').hasClass('white') ){
					// 	body.addClass('black-ui');
					// } else {
					// 	body.removeClass('black-ui');
					// }
					// // Play video if exists
					// if( $(this).prev().find('.panel-row-style-video') ){
					// 	playActiveVideo( $(this).prev().find('.panel-row-style-video') );
					// }
		            // if( !$(this).parents('.panel-grid-cell').find('.fp-next').is(":visible") ){
		            // 	$(this).parents('.panel-grid-cell').find('.fp-next').show();
		            // }
	            //}
				// Stop video if exists
				if( $(this).find('.panel-row-style-video') ){
					stopActiveVideo();
				}
				if(nextSlideIndex!=0){
					$('.fp-down').hide();
				} else {
					$('.fp-down').show();
				}
	        },
	        afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
	        	//console.log(anchorLink + " " + index + " " + slideAnchor + " " + slideIndex);
	        	updateLangLink(anchorLink, slideAnchor);
				// Show Front Page After Render
				$('#main > article').css('visibility', 'visible');
				// Check if gallery and load Alt Gallery
				if( $(this).find('.wwm_socialshare_imagewrapper') ){
					//Check if it's a galley
					var isGallery;
					if( $(this).find('.wwm_socialshare_imagewrapper:eq(0)').parent().hasClass('so-widget-sow-image-base') ){
							isGallery = true;
					}

					if(isGallery){
						$('.toggle-gallery-in').css('visibility', 'visible');
						if(!$('#main > .alt-gallery').length){
							addAltGallery();
						}
					} else {
						$('.toggle-gallery-in').css('visibility', 'hidden');
					}
				}
	        },
	        onLeave: function(index, nextIndex, direction){
	            //console.log(index + ' ' + nextIndex);
	            var currentId = $(this).attr('id').match(/\w+-\w+-/);
	            var nextId = "#" + currentId[0] + (nextIndex-1);
	            //console.log(currentId[0] + " " + nextId);
	            if( $(this).parent().find(nextId).find('.slide.active .panel-row-style').hasClass('white') ){
					body.addClass('black-ui');
				} else {
					body.removeClass('black-ui');
				}
				// Play video if exists in the next slide
				if( $(this).parent().find(nextId).find('.slide.active .panel-row-style-video') ){
					playActiveVideo( $(this).parent().find(nextId).find('.slide.active .panel-row-style-video') );
				}
				// Stop video if exists
				if( $(this).find('.slide.active .panel-row-style-video') ){
					stopActiveVideo();
				}
				// Hide/show down arrow according to current section
				if( $(this).parent().find(nextId).is(':last-of-type') ) {
					$('body').addClass('last-section');
				} else {
					$('body').removeClass('last-section');
				}
				// Insert up arrow for video as last section
				if( $(this).parent().find(nextId).find('.slide.active .panel-row-style-video') && $(this).parent().find(nextId).is(':last-of-type') && window.innerWidth < 1025 ){
					$('<a class="fp-up" href=""></a>').insertAfter('.fp-down');
					if( window.innerWidth < 520 ) {
						$('.fp-up').css('top', window.innerHeight*0.78);
					} else {
						$('.fp-up').css('top', window.innerHeight*0.84);
					}
					$('.fp-up').click(function(e){
						e.preventDefault();
						$.fn.fullpage.moveSectionUp();
					});
				} else {
					$('.fp-up').remove();
				}
	        },
	        afterLoad: function(anchorLink, index){
	            //console.log(anchorLink + " " + index);
	        	updateLangLink(anchorLink);
	        }
		});

		/* Scrolldown position and click trigger
		----------------------------------*/
		if( window.innerWidth < 1024 ) {
			$('.fp-down').css('top', window.innerHeight*0.95);
		} else {
			$('.fp-down').css('top', window.innerHeight*0.92);
		}

		if( $('body').hasClass('home') ){
			$('.fp-down').click(function(e){
				e.preventDefault();
				$.fn.fullpage.moveSectionDown();
			});
		}

		/* Images with Title
		----------------------------------*/
		$('.so-widget-image').each( function(){
			if( $(this).parent().parent().hasClass('wwm_socialshare_imagewrapper') ){
				$(this).parent().append('<span class="ref">' + $(this).attr('title') + '</span>');
			} else {
				//console.log('yo');
				if($(this).attr('title')){
					$(this).parent().append('<span class="title">' + $(this).attr('title') + '</span>');
				}
				if($(this).attr('alt')){
					$(this).next('.title').append('<small>' + $(this).attr('alt') + '</small>');
				}
			}
		})

		/* Open Menu
		----------------------------------*/
		$('.toggle-menu').click( function(){
			$('body').toggleClass('show-menu');
		})

		/* Press Gallery Title
		----------------------------------*/
	    $('.press-gallery .wwm_socialshare_imagewrapper img').each(function(){
	    	$(this).parent().append('<span class="alt">' + $(this).attr('alt') + '</span>');
	    })

		/* Alternative Gallery
		----------------------------------*/
		//var fullContents;

		function addAltGallery() {
			var photos = [];
			var title = $('.site-main > h1').text();
		    $('.wwm_socialshare_imagewrapper').each(function(){
		    	var photo = $(this).parent().html();
		    	photos.push( photo );		    })

		    $('#main').append('<article class="alt-gallery"><h3 class="widget-title">' + title + '</h3><ul></ul></article>');

		    for(i=0;i<photos.length;i++){
		    	$('.alt-gallery > ul').append('<li>' + photos[i] + '</li>');
		    }


		    // Add prettyPhoto code
		    $('.alt-gallery .wwm_socialshare_imagewrapper').each(function(){
		    	//picTitle = $(this).find('picture > .ref').text();
		    	//console.log(picTitle);
		    	//$(this).find('picture > .ref').remove();
		    	//social = $(this).find('picture > ul').html();
		    	//console.log(social);
		    	$(this).find('.wwm_social_share').remove();
		    	var picture = $(this).html();
		    	var imgUrl = $(this).find('img').attr('src');
		    	var title = 'Ref. ' + $(this).find('img').attr('title');
		    	//console.log(imgUrl);
		    	$(this).find('picture').remove();
		    	$(this).append('<a href="' + imgUrl + '" rel="prettyPhoto[gallery]">' + picture + '</a>');
		    	$(this).find('picture img').attr('alt', title);
		    })

		    $("a[rel^='prettyPhoto']").prettyPhoto({
		    	overlay_gallery: false,
		    	//allow_resize: false,
		    	social_tools: " ",
		    	keyboard_shortcuts: false,
		    	markup: '<div class="pp_pic_holder"> \
						<div class="pp_content_container"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade" class="wwm_socialshare_imagewrapper"> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"> \
										</div> \
										<div class="pp_details"> \
											<div class="ppt">&nbsp;</div> \
											<ul class="social_share"> \
												<li><a onclick="share(this)" class="wwm_facebook"></a></li> \
												<li><a onclick="share(this)" class="wwm_twitter"></a></li> \
												<li><a onclick="share(this)" class="wwm_pinit"></a></li> \
											</ul> \
											<a class="pp_close" href="#">Close</a> \
										</div> \
									</div> \
								</div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>'
		    });
		}

		$('.toggle-gallery-in').click( function(){
			//addAltGallery();
			$('html').addClass('active-scroll');
			body.addClass('black-ui').addClass('active-scroll');
		    $('#main > .page').hide();
		    $('#main > .alt-gallery').show();
		    $(this).css('visibility', 'hidden');
		    $('.toggle-gallery-out').css('visibility', 'visible');
		})

		$('.toggle-gallery-out').click( function(){
			$('html').removeClass('active-scroll');
			body.removeClass('black-ui').removeClass('active-scroll');
		    $('#main > .page').show();
		    $('#main > .alt-gallery').hide();
		    $(this).css('visibility', 'hidden');
		    $('.toggle-gallery-in').css('visibility', 'visible');
		})


	})

} )( jQuery );


function callme(img) {
    console.log(img);
}

function share(el) {
	console.log(el);
    var t = jQuery('meta[property="og:image"]').attr("content");
    var n = jQuery('meta[name="og:description"]').attr("content");
    var r = jQuery('meta[name="description"]').attr("content");
    var i = WWWM_FilterData(jQuery('meta[name="og:title"]').attr("content"));
    if (jQuery.trim(i) == "") var s = WWWM_FilterData(jQuery("title").text());
    else var s = i;
    var o = WWWM_FilterData(n);
    var u = jQuery('#pp_full_res img').attr("src");
    var a = document.location.href;
    var f = WWWM_FilterData(r);
    var l = "";
    if (jQuery.trim(u) == "") u = t;
    if (jQuery.trim(n) == "") o = s;
    if (jQuery.trim(r) == "") r = s;
    if (jQuery(el).hasClass("wwm_facebook")) {
        wwm_fb_share(s, a, u, f, o)
    }
    if (jQuery(el).hasClass("wwm_twitter")) {
        var l = "http://twitter.com/home?status=" + escape(s) + "+" + encodeURIComponent(a);
        wwm_common_share(l)
    }
    if (jQuery(el).hasClass("wwm_gplus")) {
        var l = "https://plus.google.com/share?url=" + encodeURIComponent(a);
        wwm_common_share(l)
    }
    if (jQuery(el).hasClass("wwm_pinit")) {
        var l = "http://pinterest.com/pin/create/bookmarklet/?media=" + encodeURIComponent(u) + "&url=" + encodeURIComponent(a) + "& is_video=false&description=" + o;
        wwm_common_share(l)
    }
    if (jQuery(el).hasClass("wwm_tumblr")) {
        var l = "http://www.tumblr.com/share/photo?source=" + encodeURIComponent(u) + "&caption=" + o + "&clickthru=" + encodeURIComponent(a);
        wwm_common_share(l)
    }
    if (jQuery(el).hasClass("wwm_linked")) {
        var l = "http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(a) + "&title=" + s + "&source=" + encodeURIComponent(a);
        wwm_common_share(l)
    }
}

function WWWM_FilterData(e) {
    if (jQuery.trim(e) != "") return e.replace(/[^\w\sàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ]/g, "");
    else return ""
}

function wwm_fb_share(e, t, n, r, i) {
    FB.ui({
        method: "feed",
        name: e,
        link: t,
        picture: n,
        caption: r,
        description: i
    }, function(e) {
        if (e && e.post_id) {} else {}
    })
    console.log(n);
}

function wwm_common_share(e) {
    window.open(e, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=no,height=400,width=600");
    return false
}
